
# coding: utf-8

# In[2]:

#import matplotlib.pyplot as plt
#get_ipython().magic('matplotlib inline')


# In[3]:

import tensorflow as tf
import numpy as np
import cv2
import os
import glob
import time
from sklearn.preprocessing import LabelBinarizer


def conv_layer(input, W, b, strides=1, layer_no=0):
    #weights
    weights = tf.get_variable("weight{}".format(layer_no), W,
                              initializer=tf.random_normal_initializer())
    #biases
    biases = tf.get_variable("bias{}".format(layer_no), b,
                          initializer=tf.random_normal_initializer())
    #conv layer
    x = tf.nn.conv2d(input, weights, strides=[1, strides, strides, 1], padding="SAME")
    return tf.nn.relu(x + biases)


def maxpool2d(x, strides=2):
    return tf.nn.max_pool(x, ksize=[1, strides, strides, 1], strides=[1, strides, strides, 1], padding="SAME")


def conv_net(x, weights, biases, dropout, img_h=100, img_w=100, strides_pool=2, strides_conv=1, channels=3,
             conv_layers=[], no_max_pool=[]):
    x = tf.reshape(x, shape=[-1, img_h, img_w, channels])
    layers = {}
    conv_prev = x
    for li in conv_layers:
        conv_curr = conv_layer(conv_prev, weights[li], biases[li], strides=strides_conv, layer_no=li)
        if li not in no_max_pool:
            conv_curr = maxpool2d(conv_curr, strides=strides_pool)
        conv_prev = conv_curr
        layers[li] = conv_curr
    # here is the fully connected layer
    wfc = tf.get_variable("weights-fc", weights['fc'],
                          initializer=tf.random_normal_initializer())
    bfc = tf.get_variable("biases-fc", biases['fc'],
                          initializer=tf.random_normal_initializer())
    fullyconnected = tf.reshape(conv_prev, [-1, wfc.get_shape().as_list()[0]])
    fullyconnected = tf.add(tf.matmul(fullyconnected, wfc), bfc)
    fullyconnected = tf.nn.relu(fullyconnected)
    # dropout
    fullyconnected = tf.nn.dropout(fullyconnected, dropout)
    # output
    wout = tf.get_variable("weights-out", weights['out'],
                          initializer=tf.random_normal_initializer())
    bout = tf.get_variable("biases-out", biases['out'],
                          initializer=tf.random_normal_initializer())
    out = tf.add(tf.matmul(fullyconnected, wout), bout)
    return out


def get_img_cv2(path):
    img = cv2.imread(path)
    return img


def load_set(base_path, trainset=True):
    X_train = []
    X_train_id = []
    y_train = []
    start_time = time.time()
    setpath = None
    if trainset:
        setpath = 'Training'
    else:
        setpath = 'Validation'
    base = os.path.join(base_path, setpath)
    print("start loading images - basepath: {}".format(base))
    for root, dirs, files in os.walk(base):
        for cat in dirs:
            curr_index = dirs.index(cat)
            category_path = os.path.join(base, cat, "*.jpg")
            files = glob.glob(category_path)
            for file in files:
                fname = os.path.basename(file)
                image = get_img_cv2(file)
                X_train.append(image)
                X_train_id.append(fname)
                y_train.append(curr_index)
    print("Load images complete - total time {0:.2f} sec".format((time.time() - start_time)))
    return X_train, X_train_id, y_train

def normalize_set(dataset=[], target=[], dataset_id=[]):
    print("convert to numpy array...")
    dataset = np.array(dataset, dtype=np.uint8)
    target = np.array(target, dtype=np.uint8)
    print("dataset shape {}".format(dataset.shape))
    print("Reshape...")
    dataset = dataset.transpose((0, 2, 3, 1))
    dataset = dataset.transpose((0, 1, 3, 2))
    print("new dataset shape {}".format(dataset.shape))
    print("Convert to float...")
    dataset = dataset.astype(np.float32)
    dataset = dataset / 255
    target = LabelBinarizer().fit_transform(target)
    print("new target shape {}".format(target.shape))
    return dataset, dataset_id, target


# In[4]:

X_train, X_train_id, y_train = load_set("./fruits-360")


# In[5]:

X_test, X_test_id, y_test = load_set("./fruits-360", trainset=False)


# In[6]:

X_train, X_train_id, y_train = normalize_set(X_train, y_train, X_train_id)


# In[7]:

X_test, X_test_id, y_test = normalize_set(X_test, y_test, X_test_id)


# In[8]:

n_images = 28736
learning_rate = 0.01
epochs = 50
batch_size = 500
num_batches = int(n_images / batch_size)
in_h = 100
in_w = 100
n_classes = 60
dropout = 0.55
display_step = 1
filter_h = 3
filter_w = 3
fully_connected_layer = 2048
depth_in = 3 #rgb channels
depth_out1 = 32
depth_out2 = 64
depth_out3 = 64

# In[9]:

#input and oputput layers definition
x = tf.placeholder(dtype=tf.float32, shape=[None, in_h, in_w, depth_in])
y = tf.placeholder(dtype=tf.float32, shape=[None, n_classes])
keep_prob = tf.placeholder(dtype=tf.float32)


# In[10]:

#weights of the layers
weights = {
    "1": [filter_h, filter_w, depth_in, depth_out1],
    "1a": [filter_h, filter_w, depth_out1, depth_out1],
    "2": [filter_h, filter_w, depth_out1, depth_out2],
    "2a": [filter_h, filter_w, depth_out2, depth_out3],
    "fc": [int((in_h/4) * (in_w/4) * depth_out3), fully_connected_layer],
    "out": [fully_connected_layer, n_classes]
}


# In[11]:

#biases
biases = {
    "1": [depth_out1],
    "1a": [depth_out1],
    "2": [depth_out2],
    "2a": [depth_out3],
    "fc": [fully_connected_layer],
    "out": [n_classes]
}


# In[12]:

pred = conv_net(x, weights, biases, keep_prob, in_h, in_w, strides_pool=2, strides_conv=1, channels=3, conv_layers=['1', '1a', '2', '2a'], no_max_pool=['1', '2'])
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
correct_pred = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

import random as rnd

def shuffle_set(dataset=None, target=None):
    list1 = []
    list2 = []
    index_shuf = [x for x in range(len(dataset))]
    rnd.shuffle(index_shuf)
    for i in index_shuf:
        list1.append(dataset[i,:,:,:])
        list2.append(target[i,])
    return np.array(list1, dtype=np.float32), np.array(list2, dtype=np.float32)

# In[1]:
X_train, y_train = shuffle_set(X_train, y_train)

init = tf.global_variables_initializer()
writer = tf.summary.FileWriter('/dev/shm')
writer.add_graph(tf.get_default_graph())
start_time = time.time()
saver = tf.train.Saver()
with tf.Session() as sess:
    sess.run(init)
    for i in range(epochs):
        print("Start epoch {}".format((i+1)))
        for j in range(num_batches):
            batch_x, batch_y = (X_train[j*batch_size:(j+1)*batch_size,:,:,:], y_train[j*batch_size:(j+1)*batch_size,:]) #to fix
            sess.run(optimizer, feed_dict={x: batch_x, y: batch_y, keep_prob: dropout})
            loss, acc = sess.run([cost, accuracy], feed_dict={x: batch_x, y: batch_y, keep_prob: dropout})
        if (i+1) % display_step == 0:
            print("Epoch {}, elapsed time={:.2f}sec cost={}, training accuracy={}".format(i, (time.time() - start_time), loss, acc))
            saver.save(sess, "/dev/shm/model-fruits-cnn.ckpt")
        
    print('optimization process complete after {:.2f} sec'.format((time.time() - start_time)))

