# Recognize Fruits via Sample Convolutional Neural Networks (CNNs)

This project aims at experimenting with [Tensorflow](https://www.tensorflow.org/) to build CNNs for recognizing 60 different fruits.

As a reference, we exploited the *Fruits-360* dataset provided at [https://github.com/Horea94/Fruit-Images-Dataset](https://github.com/Horea94/Fruit-Images-Dataset).

The first experimental CNN implemented in [cnn-fruits.py](./cnn-fruits.py), starting from an **input layer** of 100x100 neurons (corresponding to image resolution), is composed of
**2 convolutional layers** each featuring a **5x5 kernel** and:

* *32* feature maps in the first layer;
* *64* in the second layer.

Each convolutional layer is terminated by a max-pooling layer. At the end of the second convolutional layer the image is 1/16 of its original size.

Finally, a fully connected layer is provided with 1024 neurons and an output made of 60 neurons, one for each different fruit.

A validation is performed on a validation set ([cnn-fruits-test.ipynb](cnn-fruits-test.ipynb)) made of ~ 10.000 images leading to an overall accuracy of *0.9161583781242371*.
 